(function() {

    'use strict';

    angular.module('MainCtrl', []).controller('MainCtrl', ['$rootScope', '$scope', '$log',
        function($rootScope, $scope, $log) {
            var ctrl = this;

            ctrl.speed = 500;
            ctrl.mainViewStyle = 'anim-fade';
            ctrl.page1Style = 'anim-slide-left';
            ctrl.page2Style = 'anim-slide-left';
            ctrl.page3Style = 'anim-slide-left';

            /*
            $rootScope.$on('animStart', function() {
                $log.log('animStart');
            });

            $rootScope.$on('animEnd', function() {
                $log.log('animEnd');
            });
            */
        }
    ]);

    angular.module('ExampleApp', ['ngAnimate', 'ui.router', 'anim-in-out', 'MainCtrl'])
        .config(['$stateProvider', '$locationProvider', '$urlMatcherFactoryProvider', '$urlRouterProvider',
            function($stateProvider, $locationProvider, $urlMatcherFactoryProvider, $urlRouterProvider) {

                // $locationProvider.html5Mode(true);

                // Allow trailing slashes
                $urlMatcherFactoryProvider.strictMode(false);

                $urlRouterProvider.otherwise('/');

                $stateProvider.state('page1', {
                    url: '/',
                    views: {
                        main: {
                            templateUrl: 'application/page1.html'
                        }
                    }
                });

                $stateProvider.state('page2', {
                    url: '/page2',
                    views: {
                        main: {
                            templateUrl: 'application/page2.html'
                        }
                    }
                });

                $stateProvider.state('page3', {
                    url: '/page3',
                    views: {
                        main: {
                            templateUrl: 'application/page3.html'
                        }
                    }
                });

            }
        ]);

})();
